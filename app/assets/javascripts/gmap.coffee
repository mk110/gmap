$ ->
  initMap = ->
      map = new (google.maps.Map)(document.getElementById('map'),
        center:
          lat: -34.397
          lng: 150.644
        zoom: 6)
      #		var infoWindow = new google.maps.InfoWindow({map: map});
      map.setOptions
        minZoom: 2.5
        maxZoom: 15
      # Try HTML5 geolocation.
      if navigator.geolocation
        navigator.geolocation.getCurrentPosition ((position) ->
          pos =
            lat: position.coords.latitude
            lng: position.coords.longitude
          image = 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=30'
          beachMarker = new (google.maps.Marker)(
            position:
              lat: pos.lat
              lng: pos.lng
            map: map
            icon: image
            title: 'Current location')
          map.setCenter pos
          locations = [
            {
              lat: 39.091822
              lng: -104.062500
            }
            {
              lat: 40.404085
              lng: -103.930660
            }
            {
              lat: 39.736168
              lng: -104.999081
            }
            {
              lat: 39.792636
              lng: -104.968802
            }
            {
              lat: 39.788998
              lng: -104.916515
            }
            {
              lat: -33.727111
              lng: 150.371124
            }
            {
              lat: -33.848588
              lng: 151.209834
            }
            {
              lat: -33.851702
              lng: 151.216968
            }
            {
              lat: -34.671264
              lng: 150.863657
            }
            {
              lat: -35.304724
              lng: 148.662905
            }
            {
              lat: -36.817685
              lng: 175.699196
            }
            {
              lat: -36.828611
              lng: 175.790222
            }
            {
              lat: -37.750000
              lng: 145.116667
            }
            {
              lat: -37.759859
              lng: 145.128708
            }
            {
              lat: -37.765015
              lng: 145.133858
            }
            {
              lat: -37.770104
              lng: 145.143299
            }
            {
              lat: -37.773700
              lng: 145.145187
            }
            {
              lat: -37.774785
              lng: 145.137978
            }
            {
              lat: -37.819616
              lng: 144.968119
            }
            {
              lat: -38.330766
              lng: 144.695692
            }
            {
              lat: -39.927193
              lng: 175.053218
            }
            {
              lat: -41.330162
              lng: 174.865694
            }
            {
              lat: -42.734358
              lng: 147.439506
            }
            {
              lat: -42.734358
              lng: 147.501315
            }
            {
              lat: -42.735258
              lng: 147.438000
            }
            {
              lat: -43.999792
              lng: 170.463352
            }
          ]
          # Create an array of alphabetical characters used to label the markers.
          labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
          # Add some markers to the map.
          # Note: The code uses the JavaScript Array.prototype.map() method to
          # create an array of markers based on a given "locations" array.
          # The map() method here has nothing to do with the Google Maps API.
          markers = locations.map((location, i) ->
            new (google.maps.Marker)(
              position: location
              label: labels[i % labels.length])
          )
          # Add a marker clusterer to manage the markers.
          markerCluster = new MarkerClusterer(map, markers,
            imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
            maxZoom: 6
            minZoom: 5)
          return
        ), ->
          handleLocationError true, infoWindow, map.getCenter()
          return
      else
    # Browser doesn't support Geolocation
        handleLocationError false, infoWindow, map.getCenter()
      return

    handleLocationError = (browserHasGeolocation, infoWindow, pos) ->
      infoWindow.setPosition pos
      infoWindow.setContent if browserHasGeolocation then 'Error: The Geolocation service failed.' else 'Error: Your browser doesn\'t support geolocation.'
      return
